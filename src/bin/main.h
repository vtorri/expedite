#ifndef MAIN_H
#define MAIN_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#if defined (HAVE_EVAS_SOFTWARE_XLIB) || defined (HAVE_EVAS_XRENDER_X11) ||defined (HAVE_EVAS_OPENGL_X11)
#include <X11/Xlib.h>
#endif

#ifndef _MSC_VER
# include <sys/time.h>
#endif

#include <Evas.h>
#include <Ecore.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "ui.h"
#include "about.h"

#define EVAS_FRAME_QUEUING	// for test

#define OBNUM 128
#define LOOPS 128

extern Evas *evas;
extern int   win_w, win_h;
extern int   loops;
extern int   one_test;
extern const char *choosen_engine;
extern Eina_Bool fullscreen;
extern Eina_Bool cmp_report;
extern char *output;
extern Eina_Bool tick;

typedef struct _Expedite_Resolution Expedite_Resolution;
typedef struct _Expedite_Engine Expedite_Engine;

struct _Expedite_Resolution
{
   const char *name;
   int width;
   int height;
};

struct _Expedite_Engine
{
   const char *name;
   Eina_Bool (*init)(const char *engine, int width, int height);
   void (*loop)(void);
   void (*shutdown)(void);
};

void         srnd(void);
unsigned int rnd(void);
double       get_time(void);
const char  *build_path(const char *filename);
void         engine_loop(void);
int          engine_abort(void);

void exp_size_get(Eo *obj, int *w, int *h);
void exp_pos_get(Eo *obj, int *x, int *y);
void exp_input_pointer_pos_get(Eo *obj, int *x, int *y);

#ifdef __cplusplus
}
#endif

#define KEY_STD \
   if ((!strcmp(key, "Escape")) || (!strcmp(key, "q")) || (!strcmp(key, "Q")) || (!strcmp(key, "Return"))) \
       { \
	  _cleanup(); \
	  ui_menu(); \
       }

#define FPS_STD(x)                        \
  if (!output && (f >= loops) && (!done)) \
    {                                     \
       double fps;                        \
       fps = (double)f / t;               \
       printf("%4.2f , %s\n", fps, x);    \
       ui_fps(fps);                       \
       done = 1;                          \
    }

#define SLOW 5.0

#endif
