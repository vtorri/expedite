#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME text_styles_different_strings_start
#define NAME "Text Styles Different Strings"
#define ICON "text.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_texts[OBNUM];

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   Evas_Text_Style_Type st;
   char buf[1024];
   const char *strs[] = {
      "Big", "Smelly", "Fish", "Pants", "Octopus", "Garden", "There", "I",
	"Am", "You", "Are", "Erogenous", "We", "Stick", "Wet", "Fishy",
	"Fiddly", "Family", "Lair", "Monkeys", "Magazine"
   };

   srnd();
   st = EVAS_TEXT_STYLE_SHADOW;
   for (i = 0; i < OBNUM; i++)
     {
	o = evas_object_text_add(evas);
	o_texts[i] = o;
	efl_text_font_family_set(o, "Vera-Bold");
        efl_text_font_size_set(o, 20);
	snprintf(buf, sizeof(buf), "%s %s %s %s.",
		 strs[rnd() % (sizeof(strs) / sizeof(char *))],
		 strs[rnd() % (sizeof(strs) / sizeof(char *))],
		 strs[rnd() % (sizeof(strs) / sizeof(char *))],
		 strs[rnd() % (sizeof(strs) / sizeof(char *))]);
	efl_text_set(o, buf);
	evas_object_text_style_set(o, st);
	efl_gfx_color_set(o, 255, 255, 255, 255);
	evas_object_text_shadow_color_set(o, 0, 0, 0, 24);
	evas_object_text_glow_color_set(o, 100, 80, 40, 100);
	evas_object_text_glow2_color_set(o, 50, 10, 5, 50);
	evas_object_text_outline_color_set(o, 0, 0, 0, 255);
	efl_gfx_entity_visible_set(o, EINA_TRUE);
	st++;
	if (st > EVAS_TEXT_STYLE_FAR_SOFT_SHADOW) st = EVAS_TEXT_STYLE_SHADOW;
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_texts[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   static int k = 0;
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < OBNUM; i++)
     {
        exp_size_get(o_texts[i], &w, &h);
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + ((i + k) * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + ((i + k) * 28)) / (43.8 * SLOW)) * (w / 2);
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x, y));
     }
   ++k;
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
