#define EFL_GFX_FILTER_BETA

#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME font_effect_blur_alpha_start
#define NAME "Font Effect Blur (Alpha)"
#define ICON "text.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

#include "ui.h"


/* standard var */
static int done = 0;
extern int win_w, win_h;
/* private data */
static Evas_Object *text;
static const int MAX_BLUR = 100;

/* setup */
static void _setup(void)
{

   Evas_Object *o;
   int w,h;

   o = evas_object_text_add(evas);
   evas_object_color_set(o, 0, 0, 0, 255);
   evas_object_text_font_set(o, "Vera-Bold", 80);
   evas_object_text_text_set(o, "Font Effect");
   evas_object_show(o);
   evas_object_geometry_get(o, NULL, NULL, &w, &h);
   evas_object_move(o, (win_w / 2) - (w / 2) - MAX_BLUR, (win_h / 2) - (h / 2) - MAX_BLUR);
   text = o;

   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   evas_object_del(text);
}

/* loop - do things */
static void _loop(double t, int f)
{
   char buf[256];
   char *str = "padding_set({ %d });blur({%d,color=black });";
   sprintf(buf, str, MAX_BLUR, ((f % MAX_BLUR) + 1));
   efl_gfx_filter_program_set(text, buf, "test");

   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
