#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME line_blend_start
#define NAME "Line Blend"
#define ICON "rect.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[OBNUM];

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   srnd();
   for (i = 0; i < OBNUM; i++)
     {
	int r, g, b, a;

        o = evas_object_line_add(evas);
	o_images[i] = o;
	a = (rnd()&0xff) / 2;
	r = ((rnd()&0xff) * a) / 255;
	g = ((rnd()&0xff) * a) / 255;
	b = ((rnd()&0xff) * a) / 255;
	efl_gfx_color_set(o, r, g, b, a);
        evas_object_line_xy_set(o, ((win_w / 2) * (rnd()&0xff)) / 255, ((win_h / 2) * (rnd()&0xff)) / 255, ((win_w / 2) * (rnd()&0xff)) / 255 + (win_w / 2), ((win_h / 2) * (rnd()&0xff)) / 255 + (win_h / 2));
	efl_gfx_entity_visible_set(o, EINA_TRUE);

        
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_images[i]);
}

#define PI (double) 3.141592654

static void
_rotate_point(Evas_Coord *ox, Evas_Coord *oy, int r)
{
   double d;
   double x, y;
   double angle;

   x = *ox - (win_w / 2);
   y = *oy - (win_h / 2);

   d = sqrt(x * x + y * y);

   angle = atan((double) y / (double) x);
   if (x < 0) angle -= PI;

   switch (r & 0x3)
     {
      case 0: angle += 1 * PI / 180; break;
      case 1: angle += -1 * PI / 180; break;
      case 2: angle += 7 * PI / 180; break;
      case 3: angle += -1 * PI / 180; break;
     }

   *ox =  d * cos(angle) + (win_w / 2);
   *oy =  d * sin(angle) + (win_h / 2);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord ox1, oy1, ox2, oy2;
   Evas_Object *o;

   for (i = 0; i < OBNUM; i++)
     {
        o = o_images[i];
        evas_object_line_xy_get(o, &ox1, &oy1, &ox2, &oy2);
             
        _rotate_point(&ox1, &oy1, i);
        _rotate_point(&ox2, &oy2, i);
        evas_object_line_xy_set(o, ox1, oy1, ox2, oy2);
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
