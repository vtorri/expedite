#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME widgets_list_4_grouped_start
#define NAME "Widgets List 4 Grouped"
#define ICON "widgets.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
#define NUM 512
#define ICON_SIZE 32
static Evas_Object *o_images[NUM];
static Evas_Object *o_icons[NUM];
static Evas_Object *o_texts[NUM];

static const char *icons[] =
{
   "bug.png",
   "bulb.png",
   "camera.png",
   "colorbox.png",

   "e.png",
   "error.png",
   "flower.png",
   "house.png",

   "mushroom.png",
   "pulse.png",
   "typewriter.png",
   "warning.png",

   "watch.png"
};

static const char *labels[] =
{
   "Andrew",
   "Alex",
   "Amanda",
   "Arthur",
   "Astrid",
   "Avery",

   "Beethoven",
   "Billy",
   "Bob",
   "Bundy",

   "Candy",
   "Carsten",

   "Danny",
   "Dennis",
   "Dirk",
   "Doug",

   "Edmond",
   "Erik",

   "Fernando",
   "Frank",
   "Frederick",

   "Gabby",
   "George",
   "Gilroy",
   "Goodrich",
   "Gumby",
};

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   for (i = 0; i < NUM; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_gfx_image_border_insets_set(o, 2, 2, 2, 2);
        efl_file_simple_load(o, build_path("pan.png"), NULL);
        efl_gfx_entity_size_set(o, EINA_SIZE2D(win_w, ICON_SIZE));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_icons[i] = o;
        efl_gfx_image_border_insets_set(o, 2, 2, 2, 2);
        efl_file_simple_load(o, build_path(icons[i % 13]), NULL);
        efl_gfx_entity_size_set(o, EINA_SIZE2D(ICON_SIZE - 8, ICON_SIZE - 8));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        o = evas_object_text_add(evas);
        o_texts[i] = o;
        efl_text_font_family_set(o, "Vera-Bold");
        efl_text_font_size_set(o, 10);
        efl_text_set(o, labels[i % 26]);
        efl_gfx_color_set(o, 0, 0, 0, 255);
        efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_stack_raise_to_top(o_images[i]);
     }
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_stack_raise_to_top(o_icons[i]);
     }
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_stack_above(o_icons[i], o_icons[i - 13]);
     }
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_stack_raise_to_top(o_texts[i]);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < NUM; i++)
     {
        efl_del(o_images[i]);
        efl_del(o_icons[i]);
        efl_del(o_texts[i]);
     }
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, tw, th, cent;
   x = 0;
   y = 0 - f;
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
        efl_gfx_entity_position_set(o_icons[i], EINA_POSITION2D(x + 4, y + 4));
        exp_size_get(o_texts[i], &tw, &th);
        cent = (ICON_SIZE - th) / 2;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x + 8 + ICON_SIZE + 8, y + cent));
        y += ICON_SIZE;
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
