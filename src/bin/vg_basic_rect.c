#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME vg_basic_rect_start
#define NAME "VG Basic Rect"
#define ICON "vector.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Eo *o_shapes[OBNUM];

/* setup
 * Creating Evas Objects, each holds a vector shape.
 * Then start moving these Evas Objects. */
static void _setup(void)
{
   unsigned int i;

   for (i = 0; i < OBNUM; i++)
     {
        Efl_VG *rect;
        Eo *vector;
        double w = 70, h = 70, stroke_w = 3;

        vector = efl_add(EFL_CANVAS_VG_OBJECT_CLASS, evas);
        o_shapes[i] = vector;
        efl_gfx_entity_size_set(vector, EINA_SIZE2D(w + stroke_w * 2, h + stroke_w * 2));
        efl_gfx_entity_position_set(vector, EINA_POSITION2D(0, 0));
        efl_gfx_entity_visible_set(vector, EINA_TRUE);

        rect = efl_add(EFL_CANVAS_VG_SHAPE_CLASS, vector);
        efl_gfx_path_append_rect(rect, 0 + stroke_w, 0 + stroke_w, w, h, 10, 10);
        efl_gfx_shape_stroke_width_set(rect, stroke_w);
        efl_gfx_shape_stroke_color_set(rect, 128, 0, 128, 128);
        efl_gfx_shape_stroke_join_set(rect, EFL_GFX_JOIN_ROUND);

        efl_canvas_vg_object_root_node_set(vector, rect);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   unsigned int i;

   for (i = 0; i < OBNUM; i++) efl_del(o_shapes[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w = 200, h = 200;
   for (i = 0; i < OBNUM; i++)
     {
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (w / 2);
        efl_gfx_entity_position_set(o_shapes[i], EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
