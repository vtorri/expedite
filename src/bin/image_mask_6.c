#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_mask_6
#define NAME "Image Mask 6"
#define ICON "blend.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[OBNUM];
static Evas_Object *o_mask;

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;

   o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
   o_mask = o;
   efl_file_simple_load(o, build_path("e-logo-mask.png"), NULL);
   efl_gfx_fill_set(o, EINA_RECT(0, 0, 720, 420));
   efl_gfx_entity_size_set(o, EINA_SIZE2D(720, 420));
   efl_gfx_entity_position_set(o, EINA_POSITION2D((win_w - 720) / 2, (win_h - 420) / 2));
   efl_gfx_entity_visible_set(o, EINA_TRUE);

   for (i = 0; i < OBNUM; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_file_simple_load(o, build_path("logo.png"), NULL);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 120 / 2, 160 / 2));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(120 / 2, 160 / 2));
        evas_object_clip_set(o, o_mask);
        efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_images[i]);
   efl_del(o_mask);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < OBNUM; i++)
     {
        w = 120 / 2;
        h = 160 / 2;
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (h / 2);
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
