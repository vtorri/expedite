#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME widgets_file_icons_2_same_grouped_start
#define NAME "Widgets File Icons 2 Same Grouped"
#define ICON "widgets.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
#define NUM 512
#define ICON_SIZE 64
static Evas_Object *o_images[NUM];
static Evas_Object *o_texts[NUM];

static const char *icons[] =
{
   "e.png",
};

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   for (i = 0; i < NUM; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_file_simple_load(o, build_path(icons[i % 1]), NULL);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, ICON_SIZE, ICON_SIZE));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(ICON_SIZE, ICON_SIZE));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        o = evas_object_text_add(evas);
        o_texts[i] = o;
        efl_text_font_family_set(o, "Vera-Bold");
        efl_text_font_size_set(o, 10);
        efl_text_set(o, icons[i % 1]);
        efl_gfx_color_set(o, 0, 0, 0, 255);
        efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_stack_raise_to_top(o_images[i]);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < NUM; i++)
     {
        efl_del(o_images[i]);
        efl_del(o_texts[i]);
     }
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, tw, th, cent;
   x = 0;
   y = 0 - f;
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x + 8, y));
        exp_size_get(o_texts[i], &tw, &th);
        cent = (ICON_SIZE + 16 - tw) / 2;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x + cent, y + ICON_SIZE + 4));
        x += ICON_SIZE + 16;
        if (x > win_w)
          {
             x = 0;
             y += ICON_SIZE + 16;
          }
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
