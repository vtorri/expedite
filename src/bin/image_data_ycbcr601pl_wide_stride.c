#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_data_ycbcr601pl_wide_stride_start
#define NAME "Image Data YCbCr 601 Pointer List Wide Stride"
#define ICON "data.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_images[1];
static Eina_Slice slice[3];

/* setup */
static void _setup(void)
{
   int stride;
   FILE *f;
   int w = 320 - 16;
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_gfx_image_content_hint_set(o, EVAS_IMAGE_CONTENT_HINT_DYNAMIC);
        efl_gfx_buffer_alpha_set(o, 0);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 640, 480));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(640, 480));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        // in this test, the stride is 640 but the width changes
        slice[0].len = 640 * 480;
        slice[1].len = 320 * 240;
        slice[2].len = 320 * 240;
        f = fopen(build_path("tp.yuv"), "rb");
        if (!f) continue;
        stride = 640;
        for (int p = 0; p < 3; p++)
          {
             slice[p].mem = malloc(slice[p].len);
             fread((void *) slice[p].mem, slice[p].len, 1, f);
             efl_gfx_buffer_managed_set(o, &slice[p], EINA_SIZE2D(w, 480), stride,
                                        EFL_GFX_COLORSPACE_YCBCR422P601_PL, p);
             stride = 320;
          }
        fclose(f);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = o_images[i];
        for (int p = 0; p < 3; p++)
          {
             efl_gfx_buffer_managed_set(o, NULL, EINA_SIZE2D(640, 480), 0,
                                        EFL_GFX_COLORSPACE_YCBCR422P601_PL, p);
             free((void *) slice[p].mem);
          }
        efl_del(o);
     }
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < 1; i++)
     {
        Evas_Object *o = o_images[i];
        Eina_Slice sl[3];
        int stride;

        for (int p = 0; p < 3; p++)
          sl[p] = efl_gfx_buffer_managed_get(o, p);

        w = 640;
	h = 480;
	x = (win_w / 2) - (w / 2);
	y = (win_h / 2) - (h / 2);
        efl_gfx_entity_position_set(o, EINA_POSITION2D(x, y));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(w, h));
        efl_gfx_fill_set(o, EINA_RECT(0, 0, w, h));

        // logic here differs a bit from old expedite
        w = 320 - 16 + (f * 2) % 336;
        if (w > 640) w = 320;

        stride = 640;
        for (int p = 0; p < 3; p++)
          {
             efl_gfx_buffer_managed_set(o, &sl[p], EINA_SIZE2D(w, 480), stride,
                                        EFL_GFX_COLORSPACE_YCBCR422P601_PL, p);
             stride = 320;
          }
        efl_gfx_buffer_update_add(o, &EINA_RECT(0, 0, w, 480));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
