#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME textblock_text_fill_format
#define NAME "Textblock text_fill_format"
#define ICON "text.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_text;
static int sizes[] = {500, 600};
Evas_Coord ll, rr, tt, bb;

/* setup */
static void _setup(void)
{
   Evas_Object *o;
   Evas_Textblock_Style *st;

   o = evas_object_textblock_add(evas);
   o_text = o;
   st = evas_textblock_style_new();
   evas_textblock_style_set (st, "DEFAULT='font=Sans font_size=10 color=#000000 wrap=word align=left outline_color=#000 shadow_color=#fff8 shadow_color=#0002 glow2_color=#fe87 glow_color=#f214 underline_color=#00f linesize=40'");
   evas_object_textblock_style_set(o, st);
   evas_object_textblock_clear(o);
   evas_object_textblock_text_markup_set(o, "This test resize text block and keep style (style parsed only once)");

   efl_gfx_entity_position_set(o, EINA_POSITION2D(0, 0)); \
   efl_gfx_entity_size_set(o, EINA_SIZE2D(win_w, win_h)); \
   efl_gfx_entity_visible_set(o, EINA_TRUE);

   evas_textblock_style_free(st);

   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   efl_del(o_text);
   o_text = NULL;
}

/* loop - do things */
static void _loop(double t, int f)
{
   Evas_Coord x, y, w, h, w0, h0;
   int i = 0;

   w0 = 160;
   h0 = 120;
   w = 150 + ((1.0 + cos((double)(f + (i * 10)) / (37.4 * SLOW) )) * w0 * 2);
   h = 50 + ((1.0 + sin((double)(f + (i * 19)) / (52.6 * SLOW) )) * h0 * 2);
   x = (win_w / 2) - (w / 2);
   x += sin((double)(f + (i * 13)) / (86.7 * SLOW)) * (w0 / 2);
   y = (win_h / 2) - (h / 2);
   y += cos((double)(f + (i * 28)) / (93.8 * SLOW)) * (h0 / 2);
   efl_gfx_entity_position_set(o_text, EINA_POSITION2D(x, y));

   for (i = 0; i < 10000; i++)
     {
        evas_object_resize(o_text, sizes[i%2], sizes[i%2]);
        evas_object_textblock_style_insets_get((Efl_Canvas_Object*) o_text, &ll, &rr, &tt, &bb);
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
