#define EFL_GFX_FILTER_BETA

#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME snapshot_widgets_file_icons_start
#define NAME "Snapshot Widgets File Icons"
#define ICON "widgets.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
#define NUM 512
#define ICON_SIZE 64
static Evas_Object *o_images[NUM];
static Evas_Object *o_texts[NUM];
static Evas_Object *o_snap;

static const char *icons[] =
{
   "bug.png",
   "bulb.png",
   "camera.png",
   "colorbox.png",

   "e.png",
   "error.png",
   "flower.png",
   "house.png",

   "mushroom.png",
   "pulse.png",
   "typewriter.png",
   "warning.png",

   "watch.png"
};

/* setup */
static void _setup(void)
{
   Evas_Object *o;
   Evas_Coord x, y;
   int i;

   x = y = 0;
   for (i = 0; i < NUM; i++)
     {
        Evas_Coord tw, th, cent;

        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_file_simple_load(o, build_path(icons[i % 13]), NULL);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, ICON_SIZE, ICON_SIZE));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(ICON_SIZE, ICON_SIZE));
        efl_gfx_entity_position_set(o, EINA_POSITION2D(x + 8, y));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        o = evas_object_text_add(evas);
        o_texts[i] = o;
        efl_text_font_family_set(o, "Vera-Bold");
        efl_text_font_size_set(o, 10);
        efl_text_set(o, icons[i % 13]);
        evas_object_text_style_set(o, EVAS_TEXT_STYLE_FAR_SOFT_SHADOW);
        efl_gfx_color_set(o, 255, 255, 255, 255);
        evas_object_text_shadow_color_set(o, 0, 0, 0, 24);
        efl_gfx_entity_visible_set(o, EINA_TRUE);
        exp_size_get(o, &tw, &th);
        cent = ICON_SIZE + 16 - tw / 2;
        efl_gfx_entity_position_set(o, EINA_POSITION2D(x + cent, y + ICON_SIZE + 4));
        x += ICON_SIZE + 16;
        if (x > win_w)
          {
             x = 0;
             y += ICON_SIZE + 16;
          }
     }

   const char *filter =
      "blur { 5, dst = a, color = '#ff0' }"
     ;

   o = efl_add(EFL_CANVAS_SNAPSHOT_CLASS, evas);
   o_snap = o;
   efl_gfx_entity_visible_set(o, EINA_TRUE);
   efl_gfx_entity_position_set(o, EINA_POSITION2D(50, 50));
   efl_gfx_entity_size_set(o, EINA_SIZE2D(200, 200));
   efl_gfx_fill_set(o, EINA_RECT(0, 0, 200, 200));
   efl_gfx_filter_program_set(o, filter, "test");

   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < NUM; i++)
     {
        efl_del(o_images[i]);
        efl_del(o_texts[i]);
     }
   efl_del(o_snap);
}

/* loop - do things */
static int dx = 2;
static int dy = 2;

static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, tw, th, cent;
   x = 0;
   y = 0 - f;
   for (i = 0; i < NUM; i++)
     {
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x + 8, y));
        exp_size_get(o_texts[i], &tw, &th);
        cent = (ICON_SIZE + 16 - tw) / 2;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x + cent, y + ICON_SIZE + 4));
        x += ICON_SIZE + 16;
        if (x > win_w)
          {
             x = 0;
             y += ICON_SIZE + 16;
          }
     }

   if (1)
     {
        evas_object_geometry_get(o_snap, &x, &y, NULL, NULL);
        if (dx > 0)
          {
             if (x + dx >= win_w) dx = -dx;
          }
        else
          {
             if (x + dx <= 0) dx = -dx;
          }
        x = x + dx;

        if (dy > 0)
          {
             if (y + dy >= win_h) dy = -dy;
          }
        else
          {
             if (y + dy <= 0) dy = -dy;
          }
        x = x + dx;

        evas_object_move(o_snap, x, y);
     }

   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
