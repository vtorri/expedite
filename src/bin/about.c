#include "main.h"

static Evas_Object *o_text = NULL;

static void
_setup(void)
{
   Evas_Object *o;

   o = efl_add(EFL_CANVAS_TEXTBLOCK_CLASS, evas);
   efl_gfx_entity_position_set(o, EINA_POSITION2D(10, 40));
   efl_gfx_entity_size_set(o, EINA_SIZE2D(win_w - 20, win_h - 50));
   efl_gfx_entity_visible_set(o, EINA_TRUE);

   efl_text_font_family_set(o, "Vera");
   efl_text_font_size_set(o, 10);
   efl_text_color_set(o, 0, 0, 0, 255);
   efl_text_multiline_set(o, EINA_TRUE);
   efl_text_horizontal_align_set(o, 0.5);
   efl_text_wrap_set(o, EFL_TEXT_FORMAT_WRAP_WORD);

   efl_text_markup_set
     (o,
      "Enlightenment used to be a window manager project, but "
      "since has changed a lot to become a miniature desktop and mobile "
      "device environment all of its own. It is now made up of many "
      "components (libraries and applications) that have specific uses. "
      "It is very large, and so requires more testing and demonstration."
      "</center>"
      "</br>"
      "<center>"
      "Expedite is a full test suite for Evas, which is one of the "
      "core components of the Enlightenment Foundation Libraries. Evas "
      "handles the realtime display canvas used by EFL applications to "
      "render to many targets, including framebuffer, X11, OpenGL, memory, "
      "DirectFB and other targets to boot. It handles high level graphic "
      "layout descriptions that applications set up, dealing with the hard "
      "work of doing the drawing for them.");

   o_text = o;
   ui_fps(0.0);
}

static void
_cleanup(void)
{
   efl_del(o_text);
   o_text = NULL;
}

static void
_loop(double t EINA_UNUSED,
      int f EINA_UNUSED)
{
}

static void
_key(const char *key)
{
   if ((!strcmp(key, "Escape")) || (!strcmp(key, "q")) || (!strcmp(key, "Q")))
     {
        _cleanup();
        ui_menu();
     }
}

void
about_start(void)
{
   ui_func_set(_key, _loop, _setup);
}
